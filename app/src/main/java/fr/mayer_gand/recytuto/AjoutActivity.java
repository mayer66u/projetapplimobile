package fr.mayer_gand.recytuto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextClock;
import android.widget.TextView;

public class AjoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout);


    }

    public void click(View v){
        TextView tvlabel = (TextView) findViewById(R.id.editText);
        //RadioButton rbimportant = (RadioButton) findViewById(R.id.radioButton);
        RadioButton rbnormal = (RadioButton) findViewById(R.id.radioButton2);
        RadioButton rbfaible = (RadioButton) findViewById(R.id.radioButton3);

        String label = tvlabel.getText().toString();
        TodoItem.Tags tag = TodoItem.Tags.Important;
        if(rbnormal.isChecked()) tag = TodoItem.Tags.Normal;
        if(rbfaible.isChecked()) tag = TodoItem.Tags.Faible;
        TodoItem item = new TodoItem(tag, label );
        TodoDbHelper.addItem(item,getBaseContext());

        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
